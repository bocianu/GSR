program gsr;
uses sysutils, crt, rmt, atari;
{$r resources.rc}
const 
// timings and gui settings
    SEQ_PAUSE = 20;
    WORD_PAUSE = 10;
    CHAPTER_COUNT = 56;
    PATTERN_END_POINT = 58;
    FADE_SPEED = 2;
    WHITE = 12;

// memory locations of external resources
    RMT_PLAYER = $4c00;
    RMT_MODULE = $5000;
    CHARSET_ADDRESS = $6000;
    TEXT_ADDRESS = $6400;

// text arrays lengths
    sntCounts: array [0..10] of byte = (9, 4, 7, 7, 4, 3, 4, 3, 5, 2, 7);
    // first element of this array points to rare sequences
    // others are lengths of arrays to pick strings
var 
    strings: array [0..0] of word absolute TEXT_ADDRESS;
    s: string;
    chapter, row, col, stringPointer: byte;
    lastDir, lastRare, rareCount, lastNum: byte;
// music player
    msx: TRMT;
// pointers to music positions 
    msx_line: byte absolute RMT_PLAYER + $1cc;
    msx_pattern: byte absolute $00e6;

(**********************************************************************)
(****************************** HELPERS *******************************)
(**********************************************************************)

procedure VerticalBlankInterrupt; interrupt;
begin
    msx.Play;
    msx.Play;
    asm { jmp $E462 }; // jump to OS VBI routine
end;
    
procedure MusicInit;
begin
    msx.player := pointer(RMT_PLAYER);
    msx.modul := pointer(RMT_MODULE);
    SetIntVec(iVBL,@VerticalBlankInterrupt);
    msx.Init(6);
end;

procedure WaitForMusic(pattern, line: byte);
begin
    repeat until (msx_pattern = pattern) and (msx_line >= line);
end;

(**********************************************************************)
(************************** STRING ROUTINES ***************************)
(**********************************************************************)

function NullTermToString(ptr: word): string;
begin
    Result[0] := Char(0);
    while Peek(ptr) <> 0 do begin
        Inc(Result[0]);
        Result[byte(Result[0])] := char(Peek(ptr));
        Inc(ptr);
    end;
end;

function GetString(idx: byte): string;
begin
    Result := NullTermToString(strings[idx]);
end;

// gets particular sequence from array (except rares)
function GetSequence(num, sub: byte): string; overload;
begin
    Result := GetString(num);
    s := GetString((num + 1) * 10 + sub);
    Result := Concat(Result, s);
end;

// gets semi-random sequence from selected array of strings
// first array -> directions
// last array -> rare sequences
function GetRandomSequence(num: byte): string; overload;
var sub:byte;
begin
    if num = sntCounts[0] then begin        // if rare (last array)
        repeat 
            sub := Random(sntCounts[num + 1]);
        until sub <> lastRare;              // check if rare is different then last rare
        lastRare := sub;
        Result := GetString(100 + sub);     // get rare sequence
    end 
    else begin                              // if not rare
        repeat 
            sub := Random(sntCounts[num + 1]);  // try to pick second part
        until (num > 0) or (Odd(sub) xor Odd(lastDir)) ;   // in direction array, check if not repeated
        if num = 0 then lastDir := sub;         // if direction array then remember direction picked
        Result := GetSequence(num, sub);            
    end;
end;

// returns semi-random number of array of strings
// first array -> directions
// last array -> rare sequences
function myRandom: byte;
begin
    repeat 
        Result := Random(15);   
    until (result = 0) or (Result <> lastNum);  // only array of directions can be repeated.
    lastNum := result;       

    if Result = sntCounts[0] then begin         // if rare
        if rareCount < 2 then Result := 0       // some magic rules
            else rareCount := 0;
    end else Inc(rareCount);
    if Result > sntCounts[0] then Result := 0;
end;

function GetWord(s: string): string;
var wordLength: byte;
begin
    wordLength := 0;
    repeat
        Result[wordLength + 1] := s[stringPointer];
        Inc(wordLength);
        Inc(stringPointer);
    until (stringPointer > Length(s)) or (Result[wordLength] = ' ');
    Result[0] := char(wordLength);
end;

procedure ShowSentence(s: string);
var myWord:string;
begin
    stringPointer := 1;
    repeat 
        myWord := getWord(s);
        Inc(col, Length(myWord));
        if col > 39 then begin
            if col <> 39 then Writeln;
            Inc(row);
            col := Length(myWord);
        end;
        Write(myWord);
        Pause(WORD_PAUSE);
    until stringPointer > Length(s);
end;

(**********************************************************************)
(**************************** GUI ROUTINES ****************************)
(**********************************************************************)

procedure GuiInit;
begin
    LMARGIN := 0;
    CHBAS := Hi(CHARSET_ADDRESS);
    ClrScr;
end;

procedure SetCol(bg, fg: byte);
begin 
    COLOR1 := fg;
    COLOR2 := bg;
    COLOR4 := bg;
end;

procedure FadeToWhite;
var c: byte;
begin
    for c := 0 to WHITE do begin
        Pause(FADE_SPEED);
        COLOR2 := c;
        COLOR4 := c;
    end;
end;

procedure FadeToBlack;
var c: byte;
begin
    for c := WHITE downto 0 do begin
        Pause(FADE_SPEED);
        COLOR2 := c;
        COLOR4 := c;
    end;
end;

procedure WritelnCentered(str: string);
begin
    Writeln(Space((40 - Length(str)) div 2), str);
end;

procedure EmptyRows(count: byte);
begin 
    repeat
    Writeln;
    Dec(count);
    until count = 0;
end;

procedure ShowTitle;
begin
    SetCol(0, 0);
    ClrScr;
    EmptyRows(2);
    WritelnCentered(GetString(110));
    Writeln;
    WritelnCentered(GetString(111));
    WritelnCentered(GetString(112));
    Writeln;
    WritelnCentered(GetString(113));
    EmptyRows(4);
    WritelnCentered(GetString(114));
    Writeln;
    WritelnCentered(GetString(115));
    Writeln;
    WritelnCentered(GetString(116));
    EmptyRows(3);
    WritelnCentered(GetString(118));
    CursorOff;
    FadeToWhite;
    WaitForMusic($f4, PATTERN_END_POINT);
    FadeToBlack;
end;

procedure ShowInterlude(chapter: byte);
begin
    CursorOff;
    ClrScr;
    SetCol(WHITE, 0);
    col := 1;
    GotoXY(0, 10);
    Write('>');
    ShowSentence(getString(119 + chapter));
    Write('<');
    WaitForMusic($f4, PATTERN_END_POINT);
end;

procedure ShowChapter(chapter: byte);
begin
    ATRACT := $0;
    CursorOn;
    SetCol(0, WHITE);
    lastDir := 10;
    lastRare := 10;
    rareCount := 0;
    ClrScr;
    s:=Concat('Planeta nr ',IntToStr(chapter));
    WritelnCentered(s);     
    Writeln;
    row := 2;
    col := 0;
    repeat
        ShowSentence(GetRandomSequence(myRandom));
        if (col <> 0) and (col <> 39) then begin 
            Write(' ');
            Inc(col);
        end;
        Pause(SEQ_PAUSE);
    until row > 21;
    ShowSentence(GetSequence(8, 9));
    WaitForMusic($e4, PATTERN_END_POINT);
end;

(**********************************************************************)
(****************************** MAIN LOOP *****************************)
(**********************************************************************)

begin
    Randomize;
    MusicInit;
    GuiInit;
    repeat
        ShowTitle;
        for chapter := 1 to CHAPTER_COUNT do begin
            ShowChapter(chapter);
            FadeToWhite;
            ShowInterlude(chapter);
            FadeToBlack;
        end;
    Pause(50);
    until false;
end.
